import threading

from os import listdir
from os.path import isfile, join

import requests
from bs4 import BeautifulSoup

import math
from operator import itemgetter, attrgetter, methodcaller

import re
import string

import logging

class Iris:
    results = list()
    words = list()

    def prepareDictionaries(self):

        mypath="keywords"
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

        keywords = dict()
        files = dict()

        for name in onlyfiles:
            file = open("keywords/"+name, encoding='latin-1')

            palabras = []
            for linea in file:
                palabras += linea.split(" ")
            files[name] = len(palabras)

            for palabra in palabras:
                dictInt = keywords.get(palabra.lower(), dict())

                value = dictInt.get(name, 0)
                value += 1
                dictInt[name] = value

                keywords[palabra.lower()] = dictInt

            file.close()

        return keywords, files

    def tficf(self, term, keywords, files):
        keys = keywords[term].keys()
        tf = [0.0]*len(keys)
        icf = [0.0]*len(keys)
        r = [0.0]*len(keys)
        
        for key in keys:
            tf[list(keys).index(key)] = keywords[term][key]/files[key]
            
        total = 0
        for key in keys:
            total += files[key]
        
        for key in keys:
            icf[list(keys).index(key)] = math.log(1 + (total/keywords[term][key]) )
            
        for idx in range(len(icf)):
            r[idx] = tf[idx] * icf[idx]
        
        return r

    def loadFrequencies(self, term, keywords, files):
        r = self.tficf(term, keywords, files)
        
        keys = list(keywords[term].keys())
        lot = []
        for idx in range(len(r)):
            lot.append( (keys[idx].replace(".txt", ""),r[idx]) )
        
        return sorted(lot,key=itemgetter(1), reverse=True)
            

    def web(self, WebUrl):
        url = WebUrl
        code = requests.get(url)
        plain = code.text
        return BeautifulSoup(plain, "html.parser")

    def bio(self, WebUrl):
        s = self.web(WebUrl)
        text = ''
        try:
            text = s.find('p',{'class':'ProfileHeaderCard-bio u-dir'}).get_text()
        except AttributeError:
            text = ''
            logging.error("Error with: "+WebUrl)
        return text

    def getTweets(self, WebUrl):
        s = self.web(WebUrl)
        text = ' '
        try:
            resultSet = s.findAll("p", {"class": "tweet-text"})
            listResult = []
            for rs in resultSet:
                listResult.append(rs.get_text())
            text = ' '.join(listResult)
        except AttributeError:
            text = ' '
            logging.error("Error with: "+WebUrl)
        return text

    def cleanBio(self,bio):
        #print("Pre> ", bio)
        #bio = re.sub(r'^https?:\/\/.*[\r\n]*', ' ', bio, flags=re.MULTILINE)
        bio = re.sub(r"http\S+", "", bio)
        #print("Post> ",bio)
        return bio

    def prediction(self, terms, keywords, files):
        myroles = []
        for term in terms:
            if term.lower() in keywords.keys():
                result = self.loadFrequencies(term.lower(), keywords, files)[:1][0]
                
                myroles.append( (result[0],term.lower(),result[1]) )
        
        myroles = sorted(myroles,key=itemgetter(2), reverse=True)      
        return myroles
        
        #monarchical decision
        if len(myroles) > 0:
            return myroles[0][0]
        else:
            return 'None'

    def predictionByBio(self, url, keywords, files):
        regex = re.compile('[%s]' % re.escape(string.punctuation.replace('@','').replace('#','')))
        text = regex.sub(' ', self.bio(url))
        terms = text.split(' ')
        return self.prediction(terms, keywords, files)\

    def predictionByLoadedBio(self, bio, keywords, files):
        regex = re.compile('[%s]' % re.escape(string.punctuation.replace('@','').replace('#','')))
        text = regex.sub(' ', bio)
        terms = text.split(' ')
        return self.prediction( terms, keywords, files)

    def predictionByTweets(self, url, keywords, files):
        terms = list(set(self.getTweets(url).replace(',',' ').split(' ')))
        return self.prediction(terms, keywords, files)

    def getPrediction(self, user, url, keywords, files):

        output = open("report.csv","a+", encoding="utf-8")
        rBio = self.predictionByBio(url, keywords, files)
        rTweets = self.predictionByTweets(url, keywords, files)
        data = [user,rBio, rTweets]
        print(' '.join(data[:-1]))
        output.write( ','.join(data)+'\n')
        output.close()

    def getPredictionByLoaded(self, user, bio, keywords, files):
        rBio = self.predictionByLoadedBio(bio, keywords, files)
        data = [user,rBio]
        # for tuple in rBio:
        #     self.words.append(tuple[:2])
        self.results.append( (user,bio,rBio) )
        print(' '.join(data[:-1]))
        

    def loadFile(self, name):

        keywords, files = self.prepareDictionaries()
        
        file = open(name, "r", encoding="utf-8")
        lines = list(file.readlines())

        threads = []
        count = 0

        for idx in range(len(lines)):
            line = lines[idx]
            
            count+=1
            progress = int(count*100/len(lines))
            if progress % 10 == 0:
                print("Requests: "+str(progress)+"%")


            url = 'https://twitter.com/'
            url+= line.strip()

            t = threading.Thread(target=self.getPrediction, args=(line.strip(), url, keywords, files,))
            threads.append(t)
            t.start()
            
    
    def loadLoadedFile(self, name):
        keywords, files = self.prepareDictionaries()
        
        file = open(name, "r", encoding="utf-8")

        threads = []
        for line in file:
            values = line.strip().split(',')
            if len(values) == 2:
                t = threading.Thread(target=self.getPredictionByLoaded, args=(values[0], self.cleanBio(values[1]), keywords, files,))
                threads.append(t)
                t.start()
        
        file.close()

        output = open("report.csv","a+", encoding="utf-8")
        for result in self.results:
            output.write(result[0]+';'+str(result[1])+';'+str(result[2])+'\n')
        output.close()

        # output = open("mywords.csv","a+", encoding="utf-8")
        # for word in list(set(self.words)):
        #     delete = input(str(word)+'   Delete? y/n:   ')
        #     if delete == 'y':
        #         output.write(word[0]+';'+word[1]+'\n')
        # output.close()        

        

if __name__ == '__main__':

    iris = Iris()

    name = input('File name: ')
    while not isfile(name):
        name = input('File name: ')

    iris.loadLoadedFile(name)
    